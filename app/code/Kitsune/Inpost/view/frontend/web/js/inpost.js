define(
  [
      'ko',
      'jquery',
      'mage/storage',
      'mage/url'
  ],
  function (ko, jQuery, storage, urlBuilder) {
      'use strict';
      return function () {
        window.easyPackAsyncInit = function () {
          easyPack.init({
              defaultLocale: 'pl',
              mapType: 'google',
              searchType: 'google',
              points: {
                  types: ['parcel_locker']
              },
              map: {
                  googleKey: 'AIzaSyA5tbB8fg41ti32u8qfgIsImmeT-nu1bjM'
              }
          });
        };
        jQuery('#openInpostMap').click(function(){
          easyPack.modalMap(function(point, modal) {
            modal.closeModal();
            console.log(point);
            jQuery('#selectedInpost').text("Wybrany paczkomat: "+point.name+" - "+point.address.line1+" "+point.address.line2);
            jQuery('#openInpostMap').text("Zmień");
            jQuery('#inpostHidden').attr('value', point.name);

          }, { width: 500, height: 600 });
        });
      }      
  }
);