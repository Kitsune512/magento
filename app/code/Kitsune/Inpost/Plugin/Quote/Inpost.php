<?php

namespace Kitsune\Inpost\Plugin\Quote;

use Magento\Quote\Api\Data\TotalsInterface;
use Magento\Quote\Api\GuestCartTotalRepositoryInterface;
use Magento\Quote\Api\Data\TotalsItemInterface;
use Magento\Quote\Api\Data\TotalsItemExtensionInterfaceFactory;

class Inpost
{
    /**
     * @var TotalsItemExtensionInterfaceFactory
     */
    protected $totalsItemExtensionInterfaceFactory;

    /**
     * TotalsPlugin constructor.
     *
     * @param TotalsItemExtensionInterfaceFactory $totalsItemExtensionInterfaceFactory
     */
    public function __construct(TotalsItemExtensionInterfaceFactory $totalsItemExtensionInterfaceFactory)
    {
        $this->totalsItemExtensionInterfaceFactory = $totalsItemExtensionInterfaceFactory;
    }

    /**
     * After get items.
     *
     * @param GuestCartTotalRepositoryInterface $subject
     * @param TotalsInterface $result
     * @return TotalsInterface
     */
    public function afterGet(
        GuestCartTotalRepositoryInterface $subject,
        TotalsInterface $result
    ) {
        /** @var TotalsItemInterface $item */
        foreach ($result->getItems() as $item) {
            $extensionAttributes = $item->getExtensionAttributes();

            if (!$extensionAttributes) {
                $extensionAttributes = $this->totalsItemExtensionInterfaceFactory->create();
            }

            $extensionAttributes->setInpost('Inpost');

            $item->setExtensionAttributes($extensionAttributes);
        }

        return $result;
    }
}