<?php
namespace Kitsune\Inpost\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

	
        $setup->getConnection()->addColumn( $setup->getTable('sales_order'),'inpost',[
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'nullable' => true,
            'length' => '255',
            'default' => null,
            'comment' => 'Inpost Paczkomat',
        ]);

        $setup->getConnection()->addColumn( $setup->getTable('quote'),'inpost', [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'nullable' => true,
            'length' => '255',
            'default' => null,
            'comment' => 'Inpost Paczkomat',
        ]);


		$installer->endSetup();
	}
}